package com.brq.livraria.model;

public enum Classificacao {

	A("Alto"), 
	M("Medio"),
	B("Baixo"); 

	private String label;

	private Classificacao(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

}
