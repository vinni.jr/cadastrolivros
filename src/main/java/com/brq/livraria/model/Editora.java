package com.brq.livraria.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="editora")
public class Editora implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1634835558619738022L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "codigo_editora")
	private Long id;
	
	@Column(name = "nome_editora")
	private String nomeEditora;
	
	
	public Editora() {
		super();
	}

	public Editora(String nomeEditora) {
		super();
		this.nomeEditora = nomeEditora;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomeEditora() {
		return nomeEditora;
	}

	public void setNomeEditora(String nomeEditora) {
		this.nomeEditora = nomeEditora;
	}
	

}
