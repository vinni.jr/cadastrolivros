package com.brq.livraria.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="livro")
public class Livro implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5223406795296624859L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "codigo_livro")
	private Long id;
	
	@Column(name = "titulo_livro")
	private String tituloLivro;
	
	@Column(name = "valor_livro")
	private Double valorLivro;
	
    @Column(name = "classificacao")
    @Enumerated(EnumType.STRING)
    private Classificacao classificacao;
    
    @ManyToOne(cascade= {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "codigo_autor", referencedColumnName="codigo_autor")
    private Autor autor;
    
    @ManyToOne(cascade= {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "codigo_editora", referencedColumnName="codigo_editora")
    private Editora editora;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTituloLivro() {
		return tituloLivro;
	}

	public void setTituloLivro(String tituloLivro) {
		this.tituloLivro = tituloLivro;
	}

	public Double getValorLivro() {
		return valorLivro;
	}

	public void setValorLivro(Double valorLivro) {
		this.valorLivro = valorLivro;
	}

	public Classificacao getClassificacao() {
		return classificacao;
	}

	public void setClassificacao(Classificacao classificacao) {
		this.classificacao = classificacao;
	}

	public Autor getAutor() {
		return autor;
	}

	public void setAutor(Autor autor) {
		this.autor = autor;
	}

	public Editora getEditora() {
		return editora;
	}

	public void setEditora(Editora editora) {
		this.editora = editora;
	}
	

}
