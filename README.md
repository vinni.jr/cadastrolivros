- Porta 8085.

- Utilzando swagger para documentação.

- Testes unitarios com Junit.

- Modelo do JSON:

{
  "autor": {
    "nomeAutor": "Autor"
  },
  "editora": {
    "nomeEditora": "Ed"
  },
  "tituloLivro": "Lv",
  "valorLivro": 120
}