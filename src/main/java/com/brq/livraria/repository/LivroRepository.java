package com.brq.livraria.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.brq.livraria.model.Classificacao;
import com.brq.livraria.model.Livro;

@Repository
public interface LivroRepository extends CrudRepository<Livro, Long> {
	
	@Query("SELECT liv FROM Livro liv WHERE liv.classificacao = :classificacao")
	public List<Livro> buscarLivrosClassificacao(@Param("classificacao") Classificacao classificacao);

}
