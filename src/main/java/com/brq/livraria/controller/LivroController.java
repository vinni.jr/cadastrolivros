package com.brq.livraria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.brq.livraria.model.Classificacao;
import com.brq.livraria.model.Livro;
import com.brq.livraria.service.LivroService;

@RestController
@RequestMapping("/livro")
public class LivroController {
	
	@Autowired
	private LivroService livroService;
	
	@RequestMapping(method = RequestMethod.POST, value = "/cadastrar", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Livro> cadastrarLivro(@RequestBody Livro livro) {
		return new ResponseEntity<Livro>(livroService.cadastrar(livro), HttpStatus.CREATED);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Livro>> listarLivros() {
		List<Livro> livro = (List<Livro>) livroService.buscarTodos();
		return new ResponseEntity<List<Livro>>(livro, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/buscar-livros-classificacao/{classificacao}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Livro>> buscarLivrosClassificacao(@PathVariable Classificacao classificacao) {
		List<Livro> livro = (List<Livro>) livroService.buscarPorClassificacao(classificacao);
		return new ResponseEntity<List<Livro>>(livro, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "/alterar", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Livro> alterarLivro(@RequestBody Livro livro) {
		return new ResponseEntity<Livro>(livroService.cadastrar(livro), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/excluir/{id}")
	public ResponseEntity<Livro> excluirLivro(@PathVariable Long id) {
		livroService.excluir(id);
		return new ResponseEntity<Livro>(HttpStatus.OK);
	}

}
