package com.brq.livraria.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brq.livraria.model.Classificacao;
import com.brq.livraria.model.Livro;
import com.brq.livraria.repository.LivroRepository;

@Service
public class LivroService {
	
	@Autowired
	private LivroRepository livroRepository;
	
    public Livro cadastrar(Livro livro){
    	if(livro.getValorLivro()<= 100D) {
    		livro.setClassificacao(Classificacao.A);
    	}else if(livro.getValorLivro() >100D && livro.getValorLivro() <= 300D ) {
    		livro.setClassificacao(Classificacao.M);
    	}else {
    		livro.setClassificacao(Classificacao.B);
    	}
    	
        return livroRepository.save(livro);
    }
    
    
    public List<Livro> buscarTodos(){
    	return (List<Livro>) livroRepository.findAll();
    }
    
    public List<Livro> buscarPorClassificacao(Classificacao classificacao){
    	return (List<Livro>) livroRepository.buscarLivrosClassificacao(classificacao);
    }
    
    public void excluir(Long id){
    	livroRepository.deleteById(id);
    }
    
    public Optional<Livro> buscarPorId(Long id) {
    	Optional<Livro> optionalLv = livroRepository.findById(id);
		return optionalLv;
    }
	
	

}
