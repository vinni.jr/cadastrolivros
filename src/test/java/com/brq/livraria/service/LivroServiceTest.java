package com.brq.livraria.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

import com.brq.livraria.model.Autor;
import com.brq.livraria.model.Classificacao;
import com.brq.livraria.model.Editora;
import com.brq.livraria.model.Livro;

@SpringBootTest
@WebAppConfiguration
public class LivroServiceTest {
	
	@Autowired
	private LivroService livroService;
	
	@Test
	public void cadastrarTest(){
		Livro livro = new Livro();
		livro.setValorLivro(125.00);
		livro.setTituloLivro("Alohana");
		livro.setAutor(new Autor("Jota Teste"));
		livro.setEditora(new Editora("Mica Teste"));
		livroService.cadastrar(livro);
		Optional<Livro> livroSa = livroService.buscarPorId(1L);
		assertTrue(livroSa.isPresent());
	}
	
	@Test
	public void buscarTodosTest() {
		List<Livro> livros = livroService.buscarTodos();
		assertEquals("Alohana", livros.get(0).getTituloLivro());
	}
	
	@Test
	public void alterarTest(){
		Optional<Livro> livroSalvo = livroService.buscarPorId(1L);
		Livro livro = livroSalvo.get();
		livro.setValorLivro(500.00);
		livroService.cadastrar(livro);
		Optional<Livro> livroSa = livroService.buscarPorId(1L);
		assertTrue(livroSa.isPresent());
	}
	
	@Test
	public void buscarPorClassificacaoTest() {
		List<Livro> livros = livroService.buscarPorClassificacao(Classificacao.B);
		assertEquals(Classificacao.B, livros.get(0).getClassificacao());
	}
	
	@Test
	public void excluirTest() {
		livroService.excluir(1L);
		Optional<Livro> livroSalvo = livroService.buscarPorId(1L);
		assertFalse(livroSalvo.isPresent());
	}

}
